from singer_sdk import typing as th

from tap_appstore.client import AppStoreStream


class ReviewsStream(AppStoreStream):
    name = "Reviews"
    path = "/v1/reviewSubmissions"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList().to_dict()


class AppsStream(AppStoreStream):
    name = "Apps"
    path = "/v1/apps"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("type", th.StringType),
        th.Property("id", th.StringType),
        th.Property(
            "attributes",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("bundleId", th.StringType),
                th.Property("sku", th.StringType),
                th.Property("primaryLocale", th.StringType),
            ),
        ),
    ).to_dict()
