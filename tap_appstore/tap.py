from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_appstore.streams import AppsStream, ReviewsStream

STREAM_TYPES = [AppsStream, ReviewsStream]


class TapAppStore(Tap):
    """appstore tap class."""

    name = "tap-appstore"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "kid",
            th.StringType,
            required=True,
        ),
        th.Property(
            "iss",
            th.StringType,
            required=True,
        ),
        th.Property("aud", th.StringType, required=True),
        th.Property("private_key", th.StringType, required=True),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapAppStore.cli()
