import datetime

from authlib.jose import jwt
from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta


class AppStoreAuthenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    """Authenticator class for AppStore."""

    @property
    def token_signature_payload(self) -> dict:

        now = round(datetime.datetime.now().timestamp())
        exp = now + 1200

        return {"iss": self.config["iss"], "aud": self.config["aud"], "exp": exp}

    @property
    def token_signature_header(self) -> dict:
        header = {"alg": "ES256", "kid": self.config["kid"], "typ": "JWT"}
        return header

    @classmethod
    def create_for_stream(cls, stream) -> "AppStoreAuthenticator":
        return cls(stream=stream)

    def update_access_token(self) -> None:

        header = self.token_signature_header
        payload = self.token_signature_payload
        key = self.config["private_key"]

        token = jwt.encode(header, payload, key).decode()
        self.access_token = token
        self.last_refreshed = datetime.datetime.now().timestamp()
        self.expires_in = self.last_refreshed + 1140
